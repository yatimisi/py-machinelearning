# 資料爬取
import pandas
dfs = pandas.read_html('https://rate.bot.com.tw/xrt/quote/l6m/THB')

# print(dfs)

df = dfs[0]
df = df.iloc[:,0:6]
df.columns = ['掛牌日期','幣別','現金匯率-本行買入','現金匯率-本行賣出','即期匯率-本行買入','即期匯率-本行賣出']
df.head()
df['掛牌日期'] = pandas.to_datetime(df['掛牌日期'], format= '%Y/%m/%d')
df.set_index('掛牌日期', inplace=True)
df.sort_index(ascending = True, inplace=True)
print(df)