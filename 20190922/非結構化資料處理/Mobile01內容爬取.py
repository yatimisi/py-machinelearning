import requests
from bs4 import BeautifulSoup
headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
}
res = requests.get('https://www.mobile01.com/newslist.php', headers=headers)

soup = BeautifulSoup(res.text, 'lxml')

for news in soup.select('.l-cols__3 .c-articleCard__desc'):
    title = news.select_one('.l-articleCardDesc').text.strip()
    date = news.select_one('.l-articleCardInfo__left span').text
    author = news.select_one('.l-articleCardInfo__right span').text
    print(title, date, author, sep=', ')
    print('==================================')

