import requests
from bs4 import BeautifulSoup
from datetime import datetime

# current_time = datetime.now()

# print(current_time.strftime('%Y/%m/%d'))
# print(datetime.strptime('2019/09/20', '%Y/%m/%d'))

# res = requests.get('https://tw.news.appledaily.com/local/realtime/20190922/1637170/')

# soup = BeautifulSoup(res.text, 'lxml')

# print(soup.select_one('h1').text)
# print(soup.select_one('.ndArticle_margin p').text)
# print(soup.select_one('.ndArticle_creat').text)


def pageDetail(detailurl):
    res = requests.get(detailurl)
    soup = BeautifulSoup(res.text, 'lxml')

    title = soup.select_one('h1').text
    summary = soup.select_one('.ndArticle_margin p').text
    dt = datetime.strptime(soup.select_one('.ndArticle_creat').text, '出版時間：%Y/%m/%d %H:%M')

    return {
        'title': title,
        'summary': summary,
        'datetime': dt,
        'url': detailurl
    }

print(pageDetail('https://tw.news.appledaily.com/local/realtime/20190922/1637170/'))