import requests

url = 'https://www.railway.gov.tw/tra-tip-web/tip/tip001/tip112/querybytime'
payload = {
'_csrf': '2afcca40-20d0-41ab-a696-6697fbc73831',
'startStation': '1000-臺北',
'endStation': '1020-板橋',
'transfer': 'ONE',
'rideDate': '2019/09/22',
'startOrEndTime': 'true',
'startTime': '00:00',
'endTime': '23:59',
'trainTypeList': 'ALL',
'query': '查詢'
}

res = requests.post(url, data=payload)
print(res.text)
# print(res.json())
