import requests
from bs4 import BeautifulSoup

res = requests.get('http://tw.appledaily.com/new/realtime')

soup = BeautifulSoup(res.text, 'lxml')

for news in soup.select('li.rtddt'):
    title = news.select_one('h1').text
    category = news.select_one('h2').text
    datatime = news.select_one('time').text
    link = news.select_one('a').get('href')
    print(title, category, datatime, link)
    print('==================================')
