import requests
import pandas
from bs4 import BeautifulSoup
from datetime import datetime

news_url = 'http://tw.appledaily.com/new/realtime/{}'
newsary = []

def pageDetail(detailurl):
    res = requests.get(detailurl)
    soup = BeautifulSoup(res.text, 'lxml')

    title = soup.select_one('h1').text
    summary = soup.select_one('.ndArticle_margin p').text
    dt = datetime.strptime(soup.select_one('.ndArticle_creat').text, '出版時間：%Y/%m/%d %H:%M')
    return {
        'title': title,
        'summary': summary,
        'datetime': dt,
        'url': detailurl
    }

for page in range(1,2):
    res = requests.get(news_url.format(page))
    soup = BeautifulSoup(res.text, 'lxml')
    for news in soup.select('li.rtddt'):
        url = news.select_one('a').get('href')
        newsary.append(pageDetail(url))

newsdf = pandas.DataFrame(newsary)

